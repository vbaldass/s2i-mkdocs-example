# Quelques links

Ici vous pouvez consulter quelques links intéressants en français.

C'est aussi une bonne pratique pour rassembler tous les links individuels que vous souhaitez publier, au lieu de les mettre directement sur le `mkdocs.yml` en tant que sections de votre documentation.

- **[Accueil du CERN au format MD](https://home.cern/fr)**
- **Link en brut de l'accueil du CERN **: <https://home.cern/fr>
- **[s2i MkDocs Example dans un nouvel onglet](https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-example){target=_blank}**: Notez que cela nécessite [l'extension attr_list](https://github.com/mkdocs/mkdocs/issues/1958#issuecomment-633846122){target=_blank} dans les `markdown_extensions` de votre `mkdocs.yml`.
- **Link HTML dans un nouvel onglet**: <a href="https://fr.wikipedia.org/wiki/France" target="_blank">France</a>