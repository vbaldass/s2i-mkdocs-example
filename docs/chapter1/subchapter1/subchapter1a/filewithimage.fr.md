# Titre autre document avec une image du sous-chapitre 1 A

Ce document contient une image en français.

## Localisation automatique des médias / links / assets

![Drapeau Français](static/drapeau_francais.svg)

Cette source d'image est localisée dynamiquement tout en étant toujours référencée dans la source de démarques de la page comme `![quelquechose](image.png)`. L'image doit se trouver dans le même folder que le document.
